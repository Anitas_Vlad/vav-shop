package com.example.demo.validate;

import com.example.demo.dto.BillingDetailsDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;


@Component
public class BillingDetailsDtoValidator {

    public void validate(BillingDetailsDto billingDetailsDto, BindingResult bindingResult){
        validateFirstName(billingDetailsDto, bindingResult);
        validateLastName(billingDetailsDto, bindingResult);
        validatePhoneNumber(billingDetailsDto, bindingResult);
        validateAddress(billingDetailsDto, bindingResult);
        validatePostCode(billingDetailsDto, bindingResult);
    }

    private void validateFirstName(BillingDetailsDto billingDetailsDto, BindingResult bindingResult) {
        if (billingDetailsDto.getFirstName().length() == 0) {
            FieldError fieldError = new FieldError("billingDetailsDto", "firstName", "Please input a valid firstname.");
            bindingResult.addError(fieldError);
        }
    }

    private void validateLastName(BillingDetailsDto billingDetailsDto, BindingResult bindingResult) {
        if (billingDetailsDto.getLastName().length() == 0) {
            FieldError fieldError = new FieldError("billingDetailsDto", "lastName", "Please input a valid lastname.");
            bindingResult.addError(fieldError);
        }
    }

    private void validateAddress(BillingDetailsDto billingDetailsDto, BindingResult bindingResult) {
        if (billingDetailsDto.getLastName().length() == 0) {
            FieldError fieldError = new FieldError("billingDetailsDto", "address", "Please input a valid address.");
            bindingResult.addError(fieldError);
        }
    }

    private void validatePhoneNumber(BillingDetailsDto billingDetailsDto, BindingResult bindingResult) {
        validateNumber(billingDetailsDto, bindingResult);
    }

    private void validatePostCode(BillingDetailsDto billingDetailsDto, BindingResult bindingResult) {
        validateNumber(billingDetailsDto, bindingResult);
    }

    private void validateNumber(BillingDetailsDto billingDetailsDto, BindingResult bindingResult) {
        try {
            Integer phoneNumber = Integer.valueOf(billingDetailsDto.getPhoneNumber());
            if (Integer.parseInt(billingDetailsDto.getPhoneNumber()) < 0) {
                FieldError fieldError = new FieldError("billingDetailsDto", "phoneNumber", "Please insert a valid phone number.");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("billingDetailsDto", "phoneNumber", "Please insert a valid phone number.");
            bindingResult.addError(fieldError);
        }
    }
}
