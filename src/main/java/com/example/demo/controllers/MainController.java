package com.example.demo.controllers;

import com.example.demo.dto.*;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.service.ShoppingCartService;
import com.example.demo.service.UserService;
import com.example.demo.validate.BillingDetailsDtoValidator;
import com.example.demo.validate.ProductDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductDtoValidator productDtoValidator;
    @Autowired
    private UserService userService;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private BillingDetailsDtoValidator billingDetailsDtoValidator;

    @GetMapping("/home")
    public String homeGet(Model model) {
        List<ProductDto> productDtoList = productService.getAllAvailableProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        return "home";
    }

    @GetMapping("/product/{productId}")
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model) {
        Optional<ProductDto> optionalProductDto = productService.findProductDtoById(productId);
        if (optionalProductDto.isEmpty()) {
            return "error";
        }
        model.addAttribute("productDto", optionalProductDto.get());
        model.addAttribute("quantityDto", new QuantityDto());
        return "viewProduct";
    }

    @PostMapping(value = "/product/{productId}/add")
    public String addToCartPost(@PathVariable(value = "productId") String productId,
                                @ModelAttribute(name = "quantityDto") QuantityDto quantityDto,
                                Authentication authentication) {
        System.out.println(productId + "    " + quantityDto.getQuantity() + authentication.getName());
        shoppingCartService.addProduct(productId, quantityDto, authentication.getName());
        return "redirect:/product/" + productId;
    }

    @PostMapping(value = "/product/{productId}")
    public String viewProductDelete(@PathVariable(value = "productId") String productId) {
        productService.deleteProduct(productId);
        return "redirect:/home";
    }

    //TODO

    @GetMapping("/viewCategory/{categoryId}")
    public String viewCategory(@PathVariable(value = "categoryId") String categoryId, Model model) {
        List<ProductDto> productDtoList = productService.getProductsByCategory(categoryId.toUpperCase());
        model.addAttribute("productDtoList", productDtoList);
        return "viewCategory";
    }

    @GetMapping(value = "/login")
    public String loginGet() {
        return "login";
    }

    @GetMapping(value = "/registration")
    public String registrationGet(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "registration";
    }

    @PostMapping(value = "/registration")
    public String registrationPost(@ModelAttribute(name = "userDto") UserDto userDto) {
        userService.registerUser(userDto);
        return "redirect:/login";
    }

    @GetMapping(value = "/confirmation")
    public String confirmationGet(Authentication authentication) {
        orderService.placeOrder(authentication.getName());
        return "confirmation";
    }

    @GetMapping("/addProduct")
    public String addProductGet(Model model, @ModelAttribute("addProductMessage") String addProductMessage) {
        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPost(@ModelAttribute(name = "productDto") ProductDto productDto, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes, @RequestParam(value = "productImage", required = false) MultipartFile multipartFile) {
        productDtoValidator.validate(productDto, multipartFile, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }
        productService.addProduct(productDto, multipartFile);
        redirectAttributes.addFlashAttribute("addProductMessage", "The product was successfully added");
        return "redirect:/addProduct";
    }

    @GetMapping(value = "/buyNow")
    public String buyNowGet(Authentication authentication, Model model) {
        Map<ProductDto, QuantityPriceDto> productMap = shoppingCartService.retrieveShoppingCartContent(authentication.getName());
        model.addAttribute("productMap", productMap);
        PriceSummaryDto priceSummaryDto = shoppingCartService.computePriceSummaryDto(productMap);
        model.addAttribute("priceSummaryDto", priceSummaryDto);
        BillingDetailsDto billingDetailsDto = new BillingDetailsDto();
        model.addAttribute("billingDetailsDto", billingDetailsDto);
        return "buyNow";
    }

    @PostMapping(value = "/buyNow")
    public String buyNowPost(@ModelAttribute(name = "billingDetailsDto") BillingDetailsDto billingDetailsDto,
                             BindingResult bindingResult, Authentication authentication) {
        billingDetailsDtoValidator.validate(billingDetailsDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "buyNow";
        }
        shoppingCartService.addBillingDetails(billingDetailsDto, authentication.getName());
        return "redirect:/buyNow";
    }

    @GetMapping(value = "/orders")
    public String ordersGet(Authentication authentication, Model model) {
        Map<Map<ProductDto, QuantityPriceDto>, PriceSummaryDto> orders = orderService.retrieveOrdersMap(authentication.getName());
        model.addAttribute("orders", orders);
        return "orders";
    }

    @GetMapping(value = "/searchForUser")
    public String searchForUserGet(Model model) {
        ResetPasswordDto resetPasswordDto = new ResetPasswordDto();
        model.addAttribute("resetPasswordDto", resetPasswordDto);
        return "searchForUser";
    }

    @PostMapping(value = "searchForUser")
    public String searchForUserPost(@ModelAttribute(value = "resetPasswordDto") ResetPasswordDto resetPasswordDto,
                                    BindingResult bindingResult){

    }
}
