package com.example.demo.service;

import com.example.demo.dto.*;
import com.example.demo.mapper.BillingDetailsMapper;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShoppingCartService {
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private PriceSummaryService priceSummaryService;
    @Autowired
    private ContentRetrievingService contentRetrievingService;
    @Autowired
    private BillingDetailsMapper billingDetailsMapper;

    public void addBillingDetails(BillingDetailsDto billingDetailsDto, String userEmail){
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        shoppingCart.setBillingDetails(billingDetailsMapper.map(billingDetailsDto));
        shoppingCartRepository.save(shoppingCart);
    }

    public boolean addProduct(String productId, QuantityDto quantityDto, String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if (optionalProduct.isEmpty()) {
        }
        Product product = optionalProduct.get();
        Integer quantity = Integer.valueOf(quantityDto.getQuantity());
        if (product.getQuantityInStock() < quantity) {
            System.out.println("product is out of stock");
            return false;
        }
        for (int i = 0; i < quantity; i++) {
            shoppingCart.addProduct(product);
        }
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    public Map<ProductDto, QuantityPriceDto> retrieveShoppingCartContent(String userEmail) {
        Map<ProductDto, QuantityPriceDto> result = new LinkedHashMap<>();
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        List<Product> products = shoppingCart.getProducts();
        products.forEach(product -> contentRetrievingService.mapQuantityPriceDto(product, result));
        System.out.println(result);
        return result;
    }


    public PriceSummaryDto computePriceSummaryDto(Map<ProductDto, QuantityPriceDto> productMap) {
        return priceSummaryService.computePriceSummaryDto(productMap);
    }

}
