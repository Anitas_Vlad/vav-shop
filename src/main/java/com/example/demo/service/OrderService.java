package com.example.demo.service;

import com.example.demo.dto.*;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Order;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.model.enums.OrderStatus;
import com.example.demo.repository.OrderRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class OrderService {
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ContentRetrievingService contentRetrievingService;
    @Autowired
    private PriceSummaryService priceSummaryService;

    public void placeOrder(String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        Order order = createOrder(shoppingCart);
        shoppingCart.setProducts(new ArrayList<>());
        for (Product product : order.getProducts()) {
            product.decreaseQuantityByOne();
        }
        orderRepository.save(order);
    }

    private Order createOrder(ShoppingCart shoppingCart) {
        Order order = new Order();
        order.setOrderStatus(OrderStatus.PLACED);
        order.setProducts(shoppingCart.getProducts());
        order.setUser(shoppingCart.getUser());
        order.setBillingDetails(shoppingCart.getBillingDetails());
        order.setDateTime(LocalDateTime.now());
        return order;
    }

    //TODO Delete when sure it's useless
    public OrderDto createOrderDto(Order order) {
        return orderMapper.map(order);
    }

    public Map<Map<ProductDto, QuantityPriceDto>, PriceSummaryDto> retrieveOrdersMap(String userEmail) {
        Map<Map<ProductDto, QuantityPriceDto>, PriceSummaryDto> result = new LinkedHashMap<>();
        List<Order> orders = orderRepository.findByUserEmail(userEmail);
        orders.forEach(order -> {
            Map<ProductDto, QuantityPriceDto> mappedProducts = new LinkedHashMap<>();
            List<Product> products = order.getProducts();
            products.forEach(product -> contentRetrievingService.mapQuantityPriceDto(product, mappedProducts));
            result.put(mappedProducts, priceSummaryService.computePriceSummaryDto(mappedProducts));
        });
        System.out.println(result);
        return result;
    }
}
