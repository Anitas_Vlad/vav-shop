package com.example.demo.service;

import com.example.demo.dto.PriceSummaryDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityPriceDto;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PriceSummaryService {

    public PriceSummaryDto computePriceSummaryDto(Map<ProductDto, QuantityPriceDto> productMap) {
        int subTotalPrice = calculateSubTotalPrice(productMap);
        int shipping = 50;
        int total = calculateTotal(subTotalPrice, shipping);
        return new PriceSummaryDto(String.valueOf(subTotalPrice), String.valueOf(shipping), String.valueOf(total));
    }

    public Integer calculateSubTotalPrice(Map<ProductDto, QuantityPriceDto> productMap) {
        return productMap.values().stream()
                .map(QuantityPriceDto::getTotalPrice)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int calculateTotal(int subTotalPrice, int shipping) {
        return subTotalPrice + shipping;
    }
}
