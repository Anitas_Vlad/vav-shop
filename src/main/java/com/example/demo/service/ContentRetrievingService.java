package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityPriceDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ContentRetrievingService {

    @Autowired
    private ProductMapper productMapper;

    public void mapQuantityPriceDto(Product product, Map<ProductDto, QuantityPriceDto> result) {
        ProductDto productDto = productMapper.map(product);
        if (result.containsKey(productDto)) {
            QuantityPriceDto quantityPriceDto = result.get(productDto);
            Integer numberOfProducts = quantityPriceDto.getQuantity();
            quantityPriceDto.setQuantity(numberOfProducts + 1);
            Integer totalPrice = quantityPriceDto.getTotalPrice();
            quantityPriceDto.setTotalPrice(totalPrice + Integer.parseInt(productDto.getPrice()));
        } else {
            QuantityPriceDto quantityPriceDto = new QuantityPriceDto();
            quantityPriceDto.setTotalPrice(Integer.valueOf(productDto.getPrice()));
            quantityPriceDto.setQuantity(1);
            result.put(productDto, quantityPriceDto);
        }
    }
}
