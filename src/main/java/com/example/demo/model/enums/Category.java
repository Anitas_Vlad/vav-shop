package com.example.demo.model.enums;

import lombok.Getter;

@Getter
public enum Category {
    ACCESSORIES("Accessories"),
    ELECTRONICS("Electronics"),
    TOYS("Toys"),
    BOOKS("Books"),
    FURNITURE("Furniture"),
    BIKES("Bikes"),
    COLORS("Colors");

    private final String displayValue;
    private Category(String displayValue){
        this.displayValue = displayValue;
    }

}
