package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class BillingDetails {
    @Id
    @GeneratedValue()
    private Integer billingDetailsId;
    @OneToOne
    private Order order;
    @OneToOne
    private ShoppingCart shoppingCart;

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String address;
    private String postCode;
}
