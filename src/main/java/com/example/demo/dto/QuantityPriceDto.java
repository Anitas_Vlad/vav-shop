package com.example.demo.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuantityPriceDto {
    private Integer quantity;
    private Integer totalPrice;

}
