package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillingDetailsDto {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String address;
    private String postCode;
}
