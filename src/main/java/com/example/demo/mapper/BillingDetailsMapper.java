package com.example.demo.mapper;

import com.example.demo.dto.BillingDetailsDto;
import com.example.demo.model.BillingDetails;
import org.springframework.stereotype.Service;

@Service
public class BillingDetailsMapper {

    public BillingDetailsDto map(BillingDetails billingDetails){
        BillingDetailsDto billingDetailsDto = new BillingDetailsDto();
        billingDetailsDto.setFirstName(billingDetails.getFirstName());
        billingDetailsDto.setLastName(billingDetails.getLastName());
        billingDetailsDto.setPhoneNumber(billingDetails.getPhoneNumber());
        billingDetailsDto.setAddress(billingDetails.getAddress());
        billingDetailsDto.setEmail(billingDetails.getEmail());
        billingDetails.setPostCode(billingDetails.getPostCode());
        return billingDetailsDto;
    }

    public BillingDetails map(BillingDetailsDto billingDetailsDto){
        BillingDetails billingDetails = new BillingDetails();
        billingDetails.setAddress(billingDetailsDto.getAddress());
        billingDetails.setFirstName(billingDetailsDto.getFirstName());
        billingDetails.setLastName(billingDetails.getLastName());
        billingDetails.setPhoneNumber(billingDetailsDto.getPhoneNumber());
        billingDetails.setEmail(billingDetailsDto.getEmail());
        billingDetails.setPostCode(billingDetailsDto.getPostCode());
        return billingDetails;
    }
}
